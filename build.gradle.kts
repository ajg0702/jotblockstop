plugins {
    java
    id("com.github.johnrengelman.shadow").version("6.1.0")
}

group = "us.ajg0702"
version = "1.0.0"

repositories {
    mavenCentral()

    maven { url = uri("https://repo.codemc.io/repository/nms/") }
}

dependencies {
    compileOnly(group = "org.spigotmc", name = "spigot", version = "1.16.5-R0.1-SNAPSHOT")
}

tasks.withType<ProcessResources> {
    include("**/*.yml")
    filter<org.apache.tools.ant.filters.ReplaceTokens>(
        "tokens" to mapOf(
            "VERSION" to project.version.toString()
        )
    )
}

tasks.shadowJar {
    relocate("us.ajg0702.utils", "us.ajg0702.jotblockstop.utils")
    archiveFileName.set("${baseName}-${version}.${extension}")
}